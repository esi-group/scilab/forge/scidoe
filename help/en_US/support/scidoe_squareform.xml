<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from scidoe_squareform.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="scidoe_squareform" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>scidoe_squareform</refname>
    <refpurpose>Format distance matrix</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   S=scidoe_squareform(D)
   D=scidoe_squareform(S)
   S=scidoe_squareform(D,"tomatrix")
   D=scidoe_squareform(S,"tovector")
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>D :</term>
      <listitem><para> a 1-by-b matrix of doubles, the distance vector, where b = m*(m-1)/2</para></listitem></varlistentry>
   <varlistentry><term>S :</term>
      <listitem><para> a m-by-m matrix of doubles, the pairwise distances</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
<literal>S=scidoe_squareform(D)</literal> converts the pairwise distances
D computed by <literal>scidoe_pdist</literal> into the square matrix S,
so that S(i,j) is the distance between points i and j,
for i,j=1,2,...,m. The diagonal part of S is zero.
   </para>
   <para>
<literal>D=scidoe_squareform(S)</literal> converts the
square matrix of distances S into the vector of distances
D.
   </para>
   <para>
By default, if D has a single entry (scalar), then convert it to
a square matrix.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Compute the euclidean distance between all pairs
// of points of the matrix
X = [
0.1629447    0.8616334
0.5811584    0.3826752
0.2270954    0.4442068
0.7670017    0.7264718
0.8253974    0.1937736
]
D = scidoe_pdist(X)
S = scidoe_squareform(D)
D = scidoe_squareform(S)

// See when there is only one point.
X = [
0.3185689    0.684731
];
D1=scidoe_pdist(X)
S1=scidoe_squareform(D1)
D2=scidoe_squareform(S1)

// See the action on a scalar zero
scidoe_squareform(0.)
scidoe_squareform(0.,"tovector")
scidoe_squareform(0.,"tomatrix")

   ]]></programlisting>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><link linkend="scidoe_pdist">scidoe_pdist</link></member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>http://www.mathworks.fr/fr/help/stats/squareform.html</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2013 - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
