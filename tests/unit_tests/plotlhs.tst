// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

H = scidoe_lhsdesign(2,5);
scf();
scidoe_plotlhs(H)
//
H = scidoe_lhsdesign(3,5);
scf();
scidoe_plotlhs(H);
//
// 2D with circles
H = scidoe_lhsdesign(2,5);
scf();
scidoe_plotlhs(H,%f,%t);
